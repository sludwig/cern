# README
See also [my useful compilation for linux users][].

<!-- TOC depthFrom:2 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

## Table of Contents
-   [Useful links](#useful-links)
    -   [Phonebook](#phonebook)
    -   [Request access rights](#request-access-rights)
    -   [Obtaining a visitor card](#obtaining-a-visitor-card)
    -   [Private guided tours](#private-guided-tours)
    -   [Become a tour guide](#become-a-tour-guide)
    -   [Printing](#printing)
    -   [Geant4](#geant4)
-   [Useful commands](#useful-commands)
    -   [Geant4](#geant4)
-   [Useful Literature](#useful-literature)
    -   [M2 beam line](#m2-beam-line)
    -   [Geant4](#geant4)

[↑ top]: #table-of-contents
<br/>
<!-- /TOC -->

## Useful links
### Phonebook
The contact information of each CERN employee is available [here](https://phonebook.cern.ch/phonebook/#).

[↑ top][]

### Request access rights
Visit [ADaMS](https://apex-sso.cern.ch/pls/htmldb_edmsdb/f?p=404:1:16297711938281:::::) to request access.

[↑ top][]

### Obtaining a visitor card
The guarantor must apply for the visitor card(s) at least 48 hours before the beginning of the visit by [submitting a ticket](https://cern.service-now.com/service-portal/report-ticket.do?name=cern-visitor-card&fe=visitor-access-card). Further information can be found [here](http://admin-eguide.web.cern.ch/en/procedure/visitors-cards-access-cern-visitors-and-conference-participants).

[↑ top][]

### Private guided tours
Private guided tours has to be approved by the Visits Service. Information can be found [here](https://visits.web.cern.ch/tours/private-visits).
The boundary conditions are as follows:
-   booking max. 20 days in advance and min. 2 working days before the visit
-   max. 11 visitors
-   the [visits agenda](https://publicoutreach.cern.ch/outreach/panel#agenda/today) has to be checked
-   the organizer must [find official guides](https://apex-sso.cern.ch/pls/htmldb_cerndb1/f?p=999:4:111733233843715::NO:::) who are available and authorized to tour visitors on the proposed sites
-   the organizer must organize the transportation
-   the organizer must provide identification information for each visitor

[↑ top][]

### Become a tour guide
Check the [calendar](https://espace.cern.ch/cern-guides/Lists/Calendar/calendar.aspx) for trainings. Further information can be found [here](https://espace.cern.ch/cern-guides/Documents/Become%20a%20Guide.aspx).

[↑ top][]

### Printing
-   [CERN Printing Service][]
-   [How to install CERN printers on Ubuntu 18.04][]:<br/>
    [CUPS][] raises the following error when you use Google Chrome:
    >   Unauthorized
    >
    >   Enter your username and password or the root username and password to access this page. If you are using Kerberos authentication, make sure you have a valid Kerberos ticket.

    Use Firefox instead to avoid this issue.

[↑ top][]

### Geant4
-   [Geant4 Forum][]

[↑ top][]

## Useful commands
### Geant4
-   Source Geant4 setup using .bash_aliases on lxplus (see [my useful compilation for linux users][])
    ```bash
    geant4
    ```
    >   Without using .bash_aliases:
    >   ```bash
    >   source /cvmfs/sft.cern.ch/lcg/views/LCG_94/x86_64-centos7-gcc7-opt/setup.sh
    >   ```

-   Create new directory, copy first example and compile in build folder:
    ```bash
    mkdir examples                                                                                                                 &&
    cd examples                                                                                                                    &&
    cp -r /cvmfs/sft.cern.ch/lcg/releases/Geant4/10.04.p02-d1adc/x86_64-centos7-gcc7-opt/share/Geant4-10.4.2/examples/basic/B2 ./  &&
    mkdir B2a_build                                                                                                                &&
    cd B2a_build                                                                                                                   &&
    cmake ../B2/B2a                                                                                                                &&
    make                                                                                                                           &&
    ./exampleB2a
    ```

-   Session commands:
    -   Shoot beam particle:
        ```
        /run/beamOn 1
        ```

[↑ top][]

## Useful Literature
### M2 beam line
-   [The Upgrade Muon Beam at the SPS][]
-   [Report from the Conventional Beams Working Group to the Physics beyond Collider Study and to the European Strategy for Particle Physics][]
-   [HALO manual][]
-   Shielding studies for EHN2 with FLUKA
-   [An Introduction to the Design of High-Energy Charged Particle Beams][]
-   Particle Accelerator Physics, Springer 2015, Chapters 7.1, 7.2, 10.1f (Photolattice)
-   [Summary Report of Physics Beyond Colliders at CERN][]
-   [Physics Beyond Colliders QCD Working Group Report][]
-   [Physics Beyond Colliders at CERN Beyond the Standard Model Working Group Report][]
-   [Magnets Kit][]

[↑ top][]

### Geant4
-   [Geant4 Book For Application Developers][]
-   [Geant4 Beginners Course @ CERN][]
-   [Geant4 Advanced Course @ CERN][]
-   [Geant4 Examples module][]

[↑ top][]

<!-- Links -->
[my useful compilation for linux users]: https://gitlab.com/steffen.ludwig/linux
[CERN Printing Service]: https://printservice.web.cern.ch/printservice/UserTools/PrinterStatus.aspx
[How to install CERN printers on Ubuntu 18.04]: https://twiki.cern.ch/twiki/bin/view/Main/UbuntuPrinting
[CUPS]: http://localhost:631/admin
[Geant4 Forum]: https://geant4-forum.web.cern.ch/
[The Upgrade Muon Beam at the SPS]: https://indico.cern.ch/event/627071/contributions/2533468/attachments/1447731/2231181/M2paper1994.pdf
[Report from the Conventional Beams Working Group to the Physics beyond Collider Study and to the European Strategy for Particle Physics]: http://cds.cern.ch/record/2650989/files/PBC%20Report.pdf
[HALO manual]: https://sba.web.cern.ch/sba/Documentations/docs/halo.pdf
[An Introduction to the Design of High-Energy Charged Particle Beams]: http://gatignon.web.cern.ch/gatignon/Coet-Doble-paper.pdf
[Geant4 Book For Application Developers]: http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/ForApplicationDeveloper/html/index.html
[Geant4 Beginners Course @ CERN]: https://indico.cern.ch/event/781244/timetable/#all
[Geant4 Advanced Course @ CERN]: https://indico.cern.ch/event/789510/timetable/#all
[Geant4 Examples module]: https://gitlab.cern.ch/geant4/geant4/tree/master/examples
[Summary Report of Physics Beyond Colliders at CERN]: http://cds.cern.ch/record/2651120/files/1902.00260.pdf
[Physics Beyond Colliders QCD Working Group Report]: http://cds.cern.ch/record/2652442/files/1901.04482.pdf
[Physics Beyond Colliders at CERN Beyond the Standard Model Working Group Report]: http://cds.cern.ch/record/2652223/files/1901.09966.pdf
[Magnets Kit]: https://cds.cern.ch/record/711814/files/open-2004-003.pdf
